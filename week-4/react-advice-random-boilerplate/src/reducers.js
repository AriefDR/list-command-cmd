import { combineReducers } from 'redux';

import appReducer from '@containers/App/reducer';
import languageReducer, { storedKey as storedLangState } from '@containers/Language/reducer';
import todoReducer, { storedKeyTodo as storedKeyTodoState } from '@containers/Todo/reducers';
import themeReducer, { storedKey as storedThemeState } from '@containers/Theme/reducers';
import quoteReducer, {storedKeyQuote as storedQuoteState}from '@containers/Quote/reducers';

import { mapWithPersistor } from './persistence';

const storedReducers = {
  language: { reducer: languageReducer, whitelist: storedLangState },
  todo: { reducer: todoReducer, whitelist: storedKeyTodoState },
  theme: { reducer: themeReducer, whitelist: storedThemeState },
  advice: { reducer: quoteReducer, whitelist:  storedQuoteState},
};

const temporaryReducers = {
  app: appReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
