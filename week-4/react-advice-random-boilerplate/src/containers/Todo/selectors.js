import { createSelector } from 'reselect';
import { initialState } from '@containers/Todo/reducers';

const selectTodoState = (state) => state.todo || initialState;

const selectTodoList = createSelector(selectTodoState, (state) => state.todos);
const selectTodoFilter = createSelector(selectTodoState, (state) => state.todoFilter);
const selectTodoUpdate = createSelector(selectTodoState, (state) => state.todoUpdate);

export { selectTodoList, selectTodoFilter, selectTodoUpdate };
