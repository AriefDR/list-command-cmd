import { produce } from 'immer';

import {
  SAVE_VALUE_TODO,
  SET_TODO,
  SET_TODO_CLEAR_STATUS_DONE,
  SET_TODO_FILTER,
  SET_TODO_REMOVE,
  UPDATE_LIST_ARRAY_TODO,
  UPDATE_TODO_STATUS,
  UPDATE_VALUE_TODO,
} from '@containers/Todo/constants';

export const initialState = {
  todos: [],
  todoFilter: 'all',
  todoUpdate: { data: '', id: '' },
};

export const storedKeyTodo = ['todos'];

let todoIndex;
// eslint-disable-next-line default-param-last
const todoReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TODO:
        draft.todos = [...draft.todos, action.todos];
        break;
      case SET_TODO_FILTER:
        draft.todoFilter = action.todoFilter;
        break;
      case SET_TODO_REMOVE:
        draft.todos = draft.todos.filter((todo) => todo.id !== action.todoRemove);
        break;
      case UPDATE_TODO_STATUS:
        todoIndex = draft.todos.findIndex((todo) => todo.id === action.todoUpdate.id);
        if (todoIndex !== -1) {
          draft.todos[todoIndex].status = action.todoUpdate.status;
        }
        break;
      case SET_TODO_CLEAR_STATUS_DONE:
        draft.todos = draft.todos.filter((todo) => todo.status !== action.todoStatus);
        break;
      case UPDATE_VALUE_TODO:
        draft.todoUpdate = action.todoUpdate;
        break;
      case SAVE_VALUE_TODO:
        todoIndex = draft.todos.findIndex((todo) => todo.id === action.todoValue.id);
        draft.todos[todoIndex].data = action.todoValue.data.trim();
        break;
      case UPDATE_LIST_ARRAY_TODO:
        draft.todos = action.newArr;
        break;
      default:
        break;
    }
  });

export default todoReducer;
