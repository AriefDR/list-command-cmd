import { createSelector } from 'reselect';
import { initialState } from '@containers/Quote/reducers';

const selectAdviceState = (state) => state.advice || initialState;

const selectAdvice = createSelector(selectAdviceState, (state) => state.advice);
const selectAdviceLoading = createSelector(selectAdviceState, (state) => state.adviceLoading);
const selectAdviceError = createSelector(selectAdviceState, (state) => state.adviceError);

export { selectAdvice, selectAdviceLoading, selectAdviceError };
