import { GET_ADVICE, SET_ADVICE_ERROR, SET_ADVICE_LOADING, SET_ADVICE_STATE } from './constants';

export const setAdviceLoading = (adviceLoading) => ({
  type: SET_ADVICE_LOADING,
  adviceLoading,
});

export const setAdviceError = (adviceError) => ({
  type: SET_ADVICE_ERROR,
  adviceError,
});

export const getAdviceAction = () => ({
  type: GET_ADVICE,
});

export const setAdviceAction = (advice) => ({
  type: SET_ADVICE_STATE,
  advice,
});
