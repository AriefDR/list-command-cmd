import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_ADVICE } from './constants';
import { getRandomAdvice } from '@domain/api';
import { setAdviceAction, setAdviceError, setAdviceLoading } from './actions';

export function* getAdviceSaga() {
  yield put(setAdviceLoading(true));
  yield put(setAdviceError(null));

  try {
    const response = yield call(getRandomAdvice);
    if (response) {
      yield put(setAdviceAction(response));
      yield put(setAdviceError(null));
    }
  } catch (error) {
    console.log(error)
    yield put(setAdviceError('500, ' + error.message + '!'));
  } finally {
    yield put(setAdviceLoading(false));
  }
}

export default function* quoteSaga() {
  yield takeLatest(GET_ADVICE, getAdviceSaga);
}
