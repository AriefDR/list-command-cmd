import { produce } from 'immer';
import { SET_ADVICE_ERROR, SET_ADVICE_LOADING, SET_ADVICE_STATE } from './constants';

export const initialState = {
  advice: null,
  adviceLoading: false,
  adviceError: null,
};

export const storedKeyQuote = ['advice'];

const quoteReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_ADVICE_STATE:
        draft.advice = action.advice;
        break;
      case SET_ADVICE_LOADING:
        draft.adviceLoading = action.adviceLoading;
        break;
      case SET_ADVICE_ERROR:
        draft.adviceError = action.adviceError;
        break;

      default:
        break;
    }
  });

export default quoteReducer;
