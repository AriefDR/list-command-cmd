import MainLayout from '@layouts/MainLayout';

import Home from '@pages/Home';
import ToDo from '@pages/ToDo';
import NotFound from '@pages/NotFound';
import Quote from '@pages/Quote';

const routes = [
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home,
  //   // layout: MainLayout,
  // },
  // {
  //   path: '/',
  //   name: 'ToDo',
  //   component: ToDo,
  //   // layout: MainLayout,
  // },
  {
    path: '/',
    name: 'Quote',
    component: Quote,
    // layout: MainLayout,
  },
  { path: '*', name: 'Not Found', component: NotFound, layout: MainLayout },
];

export default routes;
