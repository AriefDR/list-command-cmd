import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import QuoteCard from '@components/QuoteCard';
import { createStructuredSelector } from 'reselect';
import qScss from './style.module.scss';
import { selectAdvice, selectAdviceError, selectAdviceLoading } from '@containers/Quote/selectors';
import { selectTheme } from '@containers/Theme/selectors';

const Quote = ({ advice, loading, errorMsg, theme }) => {
  const isDark = theme === 'dark';

  return (
    <>
      <div className={`${qScss.container} ${isDark ? qScss.dark : qScss.light}`}>
        <QuoteCard advice={advice} loading={loading} errorMsg={errorMsg} theme={theme} />
      </div>
    </>
  );
};

Quote.propTypes = {
  advice: PropTypes.object,
  loading: PropTypes.bool,
  errorMsg: PropTypes.string,
  theme: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  advice: selectAdvice,
  loading: selectAdviceLoading,
  errorMsg: selectAdviceError,
  theme: selectTheme,
});

export default connect(mapStateToProps)(Quote);
