import { all } from 'redux-saga/effects';

import appSaga from '@containers/App/saga';
import quoteSaga from '@containers/Quote/saga';

export default function* rootSaga() {
  yield all([appSaga(), quoteSaga()]);
}
