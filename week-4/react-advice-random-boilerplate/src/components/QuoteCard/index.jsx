import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import QuoteIMG from '@static/images/quote.png';
import VectorIMG from '@static/images/vector.png';
import { IconButton, Snackbar, Stack } from '@mui/material';
import CasinoIcon from '@mui/icons-material/Casino';
import { getAdviceAction } from '@containers/Quote/actions';
import { useDispatch } from 'react-redux';
import CircularProgress from '@mui/material/CircularProgress';
import qScss from './style.module.scss';
import { useEffect, useState } from 'react';

import Brightness4Icon from '@mui/icons-material/Brightness4';
import LightModeIcon from '@mui/icons-material/LightMode';
import { setTheme } from '@containers/Theme/actions';

const QuoteCard = ({ advice, loading, errorMsg, theme }) => {
  const dispatch = useDispatch();
  const [rolling, setRolling] = useState(false);
  const [displayText, setDisplayText] = useState('');
  const [currentIndex, setCurrentIndex] = useState(0);

  const [snackBar, setSnackBar] = useState({ open: false, vertical: 'top', horizontal: 'right' });

  const { vertical, horizontal, open } = snackBar;

  const handleGetQuote = () => {
    setRolling(true);
    dispatch(getAdviceAction());
  };

  const handleClose = () => {
    setSnackBar({ ...snackBar, open: false });
  };

  const handleTheme = () => {
    const newTheme = theme === 'light' ? 'dark' : 'light';
    dispatch(setTheme(newTheme));
  };

  useEffect(() => {
    if (errorMsg) {
      setSnackBar({ ...snackBar, open: true });
      setTimeout(() => {
        setSnackBar({ ...snackBar, open: false });
      }, 10000);
    }
  }, [errorMsg]);

  useEffect(() => {
    if (rolling) {
      setTimeout(() => {
        setRolling(false);
      }, 10000);
    }
  }, [rolling]);

  useEffect(() => {
    if (advice?.slip?.advice) {
      setCurrentIndex(0);
      setDisplayText('');
    }
  }, [advice]);

  useEffect(() => {
    // Typing animation effect
    if (advice?.slip?.advice && currentIndex < advice?.slip?.advice.length) {
      const timer = setTimeout(() => {
        setDisplayText((prevText) => prevText + advice?.slip?.advice[currentIndex]);
        setCurrentIndex((prevIndex) => prevIndex + 1);
      }, 80);
      return () => clearTimeout(timer);
    }
  }, [advice, currentIndex]);
  return (
    <>
      <Snackbar
        anchorOrigin={{ vertical, horizontal }}
        open={open}
        onClose={handleClose}
        message={errorMsg || 'Error While Fetching Data!'}
        key={vertical + horizontal}
      />
      <div className={qScss.overlay_head}>
        <Stack direction="row" justifyContent="flex-end" alignItems="flex-end">
          <Stack direction="row" alignItems="center" className={qScss.text_body} sx={{ fontSize: 14 }}>
            {theme == 'light' ? 'dark' : 'light'}
            <IconButton aria-label={theme} onClick={handleTheme}>
              {theme === 'light' ? <Brightness4Icon /> : <LightModeIcon />}
            </IconButton>
          </Stack>
        </Stack>
        <Card className={qScss.card}>
          {loading && (
            <Stack justifyContent="center" alignItems="center" sx={{ height: 200 }}>
              <CircularProgress color="inherit" />
            </Stack>
          )}
          {!errorMsg && !loading && (
            <CardContent>
              <h3 className={qScss.text_head}>ADVICE # {advice?.slip?.id}</h3>
              <Typography variant="body2" className={`${qScss.text_body} ${qScss.typing_animation}`}>
                "{displayText}"
              </Typography>
              <div className={qScss.garis_content}>
                <img src={VectorIMG} alt="garis" className={qScss.overlay_line} width={350} />
                <img src={QuoteIMG} alt="quote" className={qScss.overlay_quote} width={20} />
              </div>
            </CardContent>
          )}
        </Card>
        <div className={`${qScss.casino_rounded} ${loading && rolling ? qScss.rolling : ''}`}>
          <IconButton aria-label="kasino" onClick={handleGetQuote} disabled={loading}>
            <CasinoIcon sx={{ color: 'black' }} />
          </IconButton>
        </div>
      </div>
    </>
  );
};

export default QuoteCard;
