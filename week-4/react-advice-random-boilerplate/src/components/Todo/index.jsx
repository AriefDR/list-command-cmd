import { useState } from 'react';
import { useDispatch } from 'react-redux';
import CloseIcon from '@mui/icons-material/Close';
import EditIcon from '@mui/icons-material/Edit';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Typography from '@mui/material/Typography';
import { Button } from '@mui/material';
import Stack from '@mui/material/Stack';
import InputBase from '@mui/material/InputBase';
import Checkbox from '@mui/material/Checkbox';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import CheckBoxOutlineBlankIcon from '@mui/icons-material/CheckBoxOutlineBlank';
import { saveValueTodo, setTodoRemove, updateTodo, updateTodoStatus } from '@containers/Todo/actions';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities';

const STATUS_DONE = 'done';
const STATUS_NOT_DONE = 'not-done';

const SortableItem = ({ val, todoUpdate, classes }) => {
  const [openStates, setOpenStates] = useState({});
  const dispatch = useDispatch();
  const { attributes, listeners, setNodeRef, transform, transition } = useSortable({ id: val.id, data: val.data });
  const label = { inputProps: { 'aria-label': 'Checkbox' } };
  const handleRemove = (selectedRemove) => dispatch(setTodoRemove(selectedRemove));
  const handleOpenModal = (id, data) => {
    dispatch(updateTodo({ data, id }));
    setOpenStates((prevState) => ({
      ...prevState,
      [id]: true,
    }));
  };

  const handleCloseModal = (id) => {
    setOpenStates((prevState) => ({
      ...prevState,
      [id]: false,
    }));
  };

  const handleSaveValueTodo = () => {
    if (todoUpdate?.data.trim().length > 0) {
      dispatch(saveValueTodo(todoUpdate));
      handleCloseModal(todoUpdate.id);
    }
  };
  const handleUpdateStatus = (id, status) => {
    const objData = {
      id,
      status: status ? STATUS_DONE : STATUS_NOT_DONE,
    };
    dispatch(updateTodoStatus(objData));
  };
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };
  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      ref={setNodeRef}
      style={{
        transform: CSS.Transform.toString(transform),
        transition,
        cursor: 'grab',
      }}
    >
      <Stack direction="row" alignItems="center">
        <div>
          <Checkbox
            {...label}
            icon={<CheckBoxOutlineBlankIcon />}
            checkedIcon={<CheckBoxIcon />}
            checked={val.status === 'done'}
            onChange={(e) => handleUpdateStatus(val.id, e.target.checked)}
          />
        </div>
        <div
          className={`${classes.main_text} ${val.status === 'done' ? classes.coretKata : ''}`}
          {...attributes}
          {...listeners}
        >
          {val.data}
        </div>
      </Stack>
      <Stack direction="row">
        <EditIcon className={classes.icon_close} onClick={() => handleOpenModal(val.id, val.data)} />
        <CloseIcon className={classes.icon_close} onClick={() => handleRemove(val.id)} />
        <div>
          <Modal
            keepMounted
            open={Boolean(openStates[val.id])}
            onClose={() => handleCloseModal(val.id)}
            aria-labelledby="value data"
            aria-describedby="keep-mounted-modal-description"
          >
            <Box sx={style}>
              <Typography id="keep-mounted-modal-title" variant="h6" component="h2">
                Edit Your Todo
              </Typography>
              <Stack direction="row" justifyContent="space-between">
                <InputBase
                  placeholder="..."
                  value={todoUpdate.data} // Use the state variable for input value
                  onChange={(e) => dispatch(updateTodo({ data: e.target.value, id: val.id }))}
                  inputProps={{ 'aria-label': 'edit todo' }}
                />
                <Button variant="outlined" size="small" onClick={handleSaveValueTodo}>
                  Save
                </Button>
              </Stack>
            </Box>
          </Modal>
        </div>
      </Stack>
    </Stack>
  );
};

export default SortableItem;
