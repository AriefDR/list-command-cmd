import './App.css'
import fb from './assets/images/icon-facebook.svg'
import ig from './assets/images/icon-instagram.svg'
import patternLogo from './assets/images/pattern-hills.svg'
import pinterest from './assets/images/icon-pinterest.svg'
import Countdown from 'react-countdown';
import { useRef } from "react";
import { useSearchParams } from "react-router-dom";



function App() {
  const defaultDate = '2024-07-17:00:00';
  const [searchParams] = useSearchParams();
  const date = searchParams.get("date");
  const dateRef = useRef(new Date(date || defaultDate));

  const renderer = ({ days, hours, minutes, seconds }) => {
    return (
      <div className='countdown'>
        <div className='countdown-day'>
           <div className='container-kotak'>
              <div className='kotak-atas'></div>
              <h3 className='text-overlay-countdown'>{days}</h3>
              <div className="kotak-bawah"></div>
          </div>
          <h3 className='text-countdown'>DAYS</h3>
        </div>
        <div className='countdown-day'>
           <div className='container-kotak'>
              <div className='kotak-atas'></div>
              <h3 className='text-overlay-countdown'>{hours}</h3>
              <div className="kotak-bawah"></div>
          </div>
          <h3 className='text-countdown'>HOURS</h3>
        </div>
        <div className='countdown-day'>
           <div className='container-kotak'>
              <div className='kotak-atas'></div>
              <h3 className='text-overlay-countdown'>{minutes}</h3>
              <div className="kotak-bawah"></div>
          </div>
          <h3 className='text-countdown'>MINUTES</h3>
        </div>
        <div className='countdown-day'>
           <div className='container-kotak'>
              <div className='kotak-atas'></div>
              <h3 className='text-overlay-countdown'>{seconds}</h3>
              <div className="kotak-bawah"></div>
          </div>
          <h3 className='text-countdown'>SECONDS</h3>
        </div>
      </div>
    )
  }
  return (
    <div className='App'>
    <div className='container-countdown'>
      <h2 className='text-white text-comming-soon'>WE'RE LAUNCHING SOON</h2>
      {/* countdown */}
      <Countdown date={dateRef.current} renderer={renderer} />
    </div>
    <div className='footer'>
      <div className='media-social'>
       <a href=""><img src={fb} alt="" /></a>
       <a href=""><img src={ig} alt="" /></a>
       <a href=""><img src={pinterest} alt="" /></a>
      </div>
      <img src={patternLogo} alt="" className='patternLogo'/>
    </div>
  </div>
  )
}

export default App
