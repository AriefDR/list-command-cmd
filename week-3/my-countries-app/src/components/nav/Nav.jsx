import nScss from './nav.module.scss'
import Button from "@mui/material/Button";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";

const Nav = ({ darkMode, handleThemeToggle }) => {
  return (
    <div className={`${nScss.NavBox} ${darkMode ? nScss.darkMode : ""}`}>
      <div
        className={`${nScss.Text} ${darkMode ? nScss.darkModeText : ""}`}
      >
        Where in the world?
      </div>

      <div className={nScss.Theme}>
        <Button
          variant="contained"
          color="primary"
          startIcon={darkMode ? <Brightness7Icon /> : <Brightness4Icon />}
          onClick={handleThemeToggle}
        >
          {darkMode ? "Light Mode" : "Dark Mode"}
        </Button>
      </div>
    </div>
  );

}

export default Nav