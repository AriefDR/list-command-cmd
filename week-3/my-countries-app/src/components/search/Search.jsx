import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';

const BasicTextFields = ({setSearchQuery}) => {
  return (
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
      onInput={(e) => {
        setSearchQuery(e.target.value);
      }}

    >
      <TextField id="standard-basic" label="Search for a country..." variant="standard"  size="small" />
    </Box>
  );
}

export default BasicTextFields