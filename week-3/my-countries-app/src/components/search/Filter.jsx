import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';

const Filter = ({setRegion}) => {
  const handleRegionChange = (e, newValue) => {
    if (!newValue) {
      setRegion('All')
    } else {
      setRegion(newValue.value)
    }
  };
  return (
    <Autocomplete
      disablePortal
      id="combo-box-demo"
      options={regions}
      sx={{ width: 220 }}
      onChange={handleRegionChange} // Gunakan onChange untuk memperoleh nilai terpilih
      getOptionLabel={(option) => option.value} // Fungsi untuk mendapatkan label dari setiap opsi
      renderInput={(params) => (
        <TextField {...params} label="Filter by Region" size="small" />
      )}
    />
  );
}

export default Filter

const regions = [
  { value: 'Africa' },
  { value: 'Americas'},
  { value: 'Asia'},
  { value: 'Europe'},
  { value: "Oceania"},
];