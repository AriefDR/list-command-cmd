import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';

import mScss from './mediaCard.module.scss'
import { Link } from 'react-router-dom';

const MediaCard = ({countryData, darkMode}) => {
  return (
    <Link to={countryData?.name?.common.replaceAll(' ', '-')} style={{textDecoration: 'none'}} state={{
      name: countryData?.name?.common,
      darkMode: darkMode,
    }}>
      <Card sx={{ width: 300, boxShadow: 3, textDecoration: 'none' }}>
      <CardActionArea>
      <CardMedia
          component="img"
          height="140"
          image={countryData?.flags?.png}
          alt={countryData?.name?.common}
        />
        <CardContent className={darkMode ? mScss.darkModeContent : ''}>
          <Typography gutterBottom variant="h5" component="div">
            {countryData.name.common}
          </Typography>
          <Typography variant="body2" color="text.primary">
            <span className={mScss.text_bold}>Population: </span>  {countryData?.population.toLocaleString('en-US')} 
          </Typography>
          <Typography variant="body2" color="text.primary">
            <span className={mScss.text_bold}>Region: </span>  {countryData?.region} 
          </Typography>
          <Typography variant="body2" color="text.primary">
            <span className={mScss.text_bold}>Capital: </span>  {countryData?.capital} 
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
    </Link>
  );
}

export default MediaCard