import { Link, useLocation } from "react-router-dom";
import { Stack } from "@mui/material";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";

import Nav from "../nav/Nav";
import pScss from "./page.module.scss";
import { useEffect, useState } from "react";
import axios from "axios";

import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import SkeletonDetail from "../skeleton/skeletonDetail";
import ErrorsTemplate from "../error/errors";

const lightTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "light",
  },
});

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "dark",
  },
});

const Page = () => {
  const BASE_API_DETAIL_COUNTRY_NAME_URL = "https://restcountries.com/v3.1";
  const location = useLocation();
  const propsData = location.state;
  const [requestTimeout, setRequestTimeout] = useState(false);
  const [loading, setLoading] = useState(true);
  const [errorMsg, setErrorMsg] = useState(false);

  const [detailCountry, setDetailCountry] = useState([]);
  const [darkMode, setDarkMode] = useState(
    propsData ? propsData.darkMode : false
  );
  const propNativeName = detailCountry?.name?.nativeName
    ? Object.values(detailCountry.name.nativeName)
    : null;
  const propCurrencies = detailCountry?.currencies
    ? Object.values(detailCountry.currencies)
    : null;
  const propLanguages = detailCountry?.languages
    ? Object.values(detailCountry.languages)
    : null;

  useEffect(() => {
    let source = axios.CancelToken.source();

    const fetchData = async () => {
      try {
        await axios
          .get(`${BASE_API_DETAIL_COUNTRY_NAME_URL}/name/${propsData.name}`, {
            timeout: 10000,
            cancelToken: source.token,
          })
          .then((result) => {
            setRequestTimeout(false); // Request successful, reset the state
            setDetailCountry(result?.data[0]);
          });
      } catch (error) {
        if (axios.isCancel(error)) {
          console.log("Request canceled:", error.message);
        } else if (error.code === "ECONNABORTED") {
          setRequestTimeout(true); // Request timed out, set the state to true
        } else {
          setErrorMsg(true);
        }
      } finally {
        // Set loading state to false regardless of success or error
        setInterval(() => {
          setLoading(false);
        }, 1000);
      }
    };

    fetchData();

    return () => {
      source.cancel("Request canceled due to component unmount");
    };
  }, [propsData]);

  useEffect(() => {
    document.body.classList.toggle("dark-mode", darkMode);
  }, [darkMode]);

  const handleThemeToggle = () => {
    setDarkMode((prevDarkMode) => !prevDarkMode);
  };

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <CssBaseline />
      {requestTimeout ? (
        <ErrorsTemplate
          msg="Request Timeout!"
          codeError={500}
          txtBtn={"RELOAD PAGE"}
        />
      ) : (
        <>
          {errorMsg && ( // Conditional rendering for error message
            <ErrorsTemplate msg="" codeError={404} txtBtn={"BACK TO HOME"} />
          )}
          {!loading && !errorMsg && (
            <>
              <Nav darkMode={darkMode} handleThemeToggle={handleThemeToggle} />
              <div
                className={`${pScss.container} ${
                  darkMode ? pScss.darkModeContent : ""
                }`}
              >
                <Link to={"/"} state={darkMode}>
                  <Button
                    className={
                      darkMode ? pScss.btn_darkMode : pScss.btn_lightMode
                    }
                    size="medium"
                    variant="outlined"
                    sx={{ mb: 5 }}
                  >
                    {"<= Back"}
                  </Button>
                </Link>
                <Stack
                  direction={{
                    xs: "column",
                    sm: "column",
                    md: "column",
                    lg: "row",
                  }}
                  spacing={3}
                  justifyContent="center"
                  alignItems="center"
                  sx={{ pb: 3 }}
                >
                  {/* belum */}
                  <Box className={pScss.container_content}>
                    <img
                      src={detailCountry?.flags?.png}
                      alt=""
                      className={pScss.imgFlag}
                    />
                  </Box>
                  <Stack
                    direction={{
                      xs: "column",
                      sm: "column",
                      md: "column",
                      lg: "row",
                    }}
                    sx={{ pt: 5 }}
                  >
                    <Stack direction="column" spacing={3} sx={{ pt: 1 }}>
                      <h1>{detailCountry?.name?.common}</h1>
                      <Stack
                        direction={{
                          xs: "column",
                          sm: "column",
                          md: "column",
                          lg: "row",
                        }}
                      >
                        <Stack direction="column" spacing={1} sx={{ pt: 3 }}>
                          <h3>
                            Native Name:{" "}
                            <span className={pScss.text_small}>
                              {propNativeName &&
                                propNativeName
                                  .map((val) => val.common)
                                  .join(", ")}
                            </span>
                          </h3>
                          <h3>
                            Population:{" "}
                            <span className={pScss.text_small}>
                              {detailCountry?.population
                                ? detailCountry.population.toLocaleString(
                                    "en-US"
                                  )
                                : "N/A"}
                            </span>
                          </h3>
                          <h3>
                            Region:{" "}
                            <span className={pScss.text_small}>
                              {detailCountry?.region}
                            </span>
                          </h3>
                          <h3>
                            Sub Region:{" "}
                            <span className={pScss.text_small}>
                              {detailCountry?.subregion}
                            </span>
                          </h3>
                          <h3>
                            Capital:{" "}
                            <span className={pScss.text_small}>
                              {detailCountry?.capital}
                            </span>
                          </h3>
                        </Stack>
                        <Stack direction="column" spacing={1} sx={{ pt: 3 }}>
                          <h3>
                            Top Level Domain:{" "}
                            <span className={pScss.text_small}>
                              {detailCountry?.tld}
                            </span>
                          </h3>
                          <h3>
                            Currencies:{" "}
                            <span className={pScss.text_small}>
                              {propCurrencies &&
                                propCurrencies
                                  .map((val) => val.name)
                                  .join(", ")}
                            </span>
                          </h3>
                          <h3>
                            Languages:{" "}
                            <span className={pScss.text_small}>
                              {propLanguages &&
                                propLanguages
                                  .map((val, i, arr) => {
                                    if (i === 0 && arr.length - 1 !== i) {
                                      return val + ",";
                                    } else {
                                      return val;
                                    }
                                  })
                                  .join(" ")}
                            </span>
                          </h3>
                        </Stack>
                      </Stack>
                      <Stack direction="column">
                        <h3>Borders: </h3>
                        <Stack direction="row" spacing={2}>
                          {detailCountry?.borders &&
                            detailCountry?.borders.map((val, i) => (
                              <div className={pScss.border_country} key={i}>
                                {val}
                              </div>
                            ))}
                        </Stack>
                      </Stack>
                    </Stack>
                  </Stack>
                </Stack>
              </div>
            </>
          )}
          {loading && (
            <>
              <div
                className={`${pScss.container} ${
                  darkMode ? pScss.darkModeContent : ""
                }`}
              >
                <SkeletonDetail pScss={pScss} />
              </div>
            </>
          )}
        </>
      )}
    </ThemeProvider>
  );
};

export default Page;
