import {
  Card,
  CardActionArea,
  CardContent,
  Skeleton,
  Stack,
} from "@mui/material";

const SkeletonProres = () => {
  return (
    <Card
      sx={{
        width: 300,
        boxShadow: 3,
        textDecoration: "none",
        pt: 3,
        height: 250,
      }}
    >
      <CardActionArea>
        <Stack justifyContent="center" alignItems="center">
          <Skeleton variant="rounded" width={250} height={100} />
        </Stack>
        <CardContent>
          <Stack spacing={1} justifyContent="center" alignItems="center">
            <Skeleton variant="text" width={250} height={20} />
            <Skeleton variant="text" width={250} height={20} />
            <Skeleton variant="text" width={250} height={20} />
          </Stack>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default SkeletonProres;
