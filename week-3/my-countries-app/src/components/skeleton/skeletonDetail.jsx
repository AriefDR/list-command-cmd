import { Box, Skeleton, Stack } from "@mui/material";

const SkeletonDetail = ({ pScss }) => {
  return (
    <div className={pScss.container}>
      <Stack
        direction={{ xs: "column", sm: "column", md: "column", lg: "row" }}
        spacing={3}
        justifyContent="center"
        alignItems="center"
      >
        <Box className={pScss.container_content}>
          <Skeleton variant="rounded" width={700} height={360} />
        </Box>
        <Stack direction="column" spacing={6} sx={{ pt: 1 }}>
          <Stack direction="column">
            <Skeleton variant="text" width={250} height={80} />
            <Stack
              direction={{
                xs: "column",
                sm: "column",
                md: "column",
                lg: "row",
              }}
              spacing={3}
            >
              <Stack direction="column" spacing={1} sx={{ pt: 3 }}>
                <Skeleton variant="text" height={20} />
                <Skeleton variant="text" width={250} height={20} />
                <Skeleton variant="text" width={250} height={20} />
                <Skeleton variant="text" width={250} height={20} />
                <Skeleton variant="text" width={250} height={20} />
              </Stack>
              <Stack direction="column" spacing={1} sx={{ pt: 3 }}>
                <Skeleton variant="text" width={250} height={20} />
                <Skeleton variant="text" width={250} height={20} />
                <Skeleton variant="text" width={250} height={20} />
              </Stack>
            </Stack>
          </Stack>
          <Stack direction="column">
            <Skeleton variant="text" width={250} height={20} />
            <Stack direction="row" spacing={2}>
              <Skeleton variant="text" width={50} height={20} />
              <Skeleton variant="text" width={50} height={20} />
              <Skeleton variant="text" width={50} height={20} />
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    </div>
  );
};

export default SkeletonDetail;
