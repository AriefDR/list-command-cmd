import eScss from "./style.module.scss";
const ErrorsTemplate = ({ msg, codeError, txtBtn }) => {
  const handleReload = () => {
    window.location.href = "/";
  };

  return (
    <section className={eScss.wrapper}>
      <div className={eScss.container}>
        <div id="scene" className={eScss.scene} data-hover-only="false">
          <div className={eScss.circle} data-depth="1.2"></div>

          <div className={eScss.one} data-depth="0.9">
            <div className={eScss.content}>
              <span className={eScss.piece}></span>
              <span className={eScss.piece}></span>
              <span className={eScss.piece}></span>
            </div>
          </div>

          <div className={eScss.two} data-depth="0.60">
            <div className={eScss.content}>
              <span className={eScss.piece}></span>
              <span className={eScss.piece}></span>
              <span className={eScss.piece}></span>
            </div>
          </div>

          <div className={eScss.three} data-depth="0.40">
            <div className={eScss.content}>
              <span className={eScss.piece}></span>
              <span className={eScss.piece}></span>
              <span className={eScss.piece}></span>
            </div>
          </div>

          <p className={eScss.p404} data-depth="0.50">
            {codeError}
          </p>
          <p className={eScss.p404} data-depth="0.10">
            {codeError}
          </p>
        </div>

        <div className={eScss.text}>
          <article>
            <p>
              Uh oh! Looks like you got lost. <br />
              {msg}
            </p>
            <button onClick={handleReload}>{txtBtn}</button>
          </article>
        </div>
      </div>
    </section>
  );
};

export default ErrorsTemplate;
