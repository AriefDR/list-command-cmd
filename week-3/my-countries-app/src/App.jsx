import { useEffect, useState } from "react";
import axios from "axios";

import aScss from "./App.module.scss";
import Nav from "./components/nav/Nav";
import Search from "./components/search/Search";
import Filter from "./components/search/Filter";
import MediaCard from "./components/card/MediaCard";

import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { useLocation } from "react-router-dom";
import ErrorsTemplate from "./components/error/errors";
import SkeletonProres from "./components/skeleton/skeletonCard";

const lightTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "light",
  },
});

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "dark",
  },
});

function App() {
  const BASE_API_URL = "https://restcountries.com/v3.1";
  const [listCountries, setListCountries] = useState([]);
  const [region, setRegion] = useState("All");
  const [searchQuery, setSearchQuery] = useState("");
  const location = useLocation();
  const propsDarkMode = location.state;
  const [darkMode, setDarkMode] = useState(
    propsDarkMode ? propsDarkMode : false
  );
  const [requestTimeout, setRequestTimeout] = useState(false);
  const [loading, setLoading] = useState(true);
  const [errorMsg, setErrorMsg] = useState(false);

  useEffect(() => {
    let source = axios.CancelToken.source();

    const fetchData = async () => {
      try {
        await axios
          .get(`${BASE_API_URL}/all`, {
            timeout: 10000,
            cancelToken: source.token,
          })
          .then((result) => {
            setRequestTimeout(false); // Request successful, reset the state
            setListCountries(result?.data);
          });
      } catch (error) {
        if (axios.isCancel(error)) {
          console.log("Request canceled:", error.message);
        } else if (error.code === "ECONNABORTED") {
          setRequestTimeout(true); // Request timed out, set the state to true
        } else {
          setErrorMsg(true);
        }
      } finally {
        // Set loading state to false regardless of success or error
        setInterval(() => {
          setLoading(false);
        }, 1000);
      }
    };

    fetchData();

    return () => {
      source.cancel("Request canceled due to component unmount");
    };
  }, []);

  useEffect(() => {
    document.body.classList.toggle("dark-mode", darkMode);
  }, [darkMode]);

  const handleThemeToggle = () => {
    setDarkMode((prevDarkMode) => !prevDarkMode);
  };

  const filteredData = listCountries.filter((country) => {
    return (
      country?.name?.common.toLowerCase().includes(searchQuery.toLowerCase()) &&
      (region == "All" || country?.region.toLowerCase() == region.toLowerCase())
    );
  });

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <CssBaseline />
      {requestTimeout ? (
        <ErrorsTemplate
          msg="Request Timeout!"
          codeError={500}
          txtBtn={"RELOAD PAGE"}
        />
      ) : (
        <>
          {errorMsg && ( // Conditional rendering for error message
            <ErrorsTemplate
              msg="Internal Server Error!"
              codeError={500}
              txtBtn={"RELOAD PAGE"}
            />
          )}
          {!loading && !errorMsg && (
            <>
              <Nav darkMode={darkMode} handleThemeToggle={handleThemeToggle} />
              <div
                className={`${aScss.container} ${
                  darkMode ? aScss.darkModeContent : ""
                }`}
              >
                <div className={aScss.search}>
                  <Search setSearchQuery={setSearchQuery} />
                  <Filter setRegion={setRegion} />
                </div>
                <div className={aScss.content}>
                  {filteredData.length == 0
                    ? "Uh oh! Looks like you got lost."
                    : filteredData.map((country, i) => (
                        <MediaCard
                          key={i}
                          countryData={country}
                          darkMode={darkMode}
                        />
                      ))}
                </div>
              </div>
            </>
          )}
          {loading && (
            <>
              <Nav darkMode={darkMode} handleThemeToggle={handleThemeToggle} />
              <div
                className={`${aScss.container} ${
                  darkMode ? aScss.darkModeContent : ""
                }`}
              >
                <div className={aScss.search}>
                  <Search setSearchQuery={setSearchQuery} />
                  <Filter setRegion={setRegion} />
                </div>
                <div className={aScss.content}>
                  {Array.from({ length: 40 }).map((_, index) => (
                    <SkeletonProres key={index} />
                  ))}
                </div>
              </div>
            </>
          )}
        </>
      )}
    </ThemeProvider>
  );
}

export default App;
