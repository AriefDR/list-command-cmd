const emotify = (str) => {
    let emot = '';
    switch (str.split(' ')[2]) {
        case 'smile':
            emot = ':D';
            break;
        case 'grin':
            emot = ':)';
            break;
        case 'sad':
            emot = ':(';
            break;
        case 'mad':
            emot = ':P';
            break;
        default:
            break;
    }
    return "Make me " + emot;
}

console.table([
    emotify("Make me smile"),
    emotify("Make me grin"),
    emotify("Make me sad"),
    emotify("Make me smile"),
    emotify("Make me grin"),
    emotify("Make me sad"),
    emotify("Make me mad"),
])


const makeTitle = (str) => {
    let arr = str.split(' ')
    for (var i = 0; i < arr.length; i++) {
        arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
    }

    return arr.toString().replaceAll(',', ' ');
}


console.table([
    makeTitle("This is a title"),
    makeTitle("capitalize every word"),
    makeTitle("I Like Pizza"),
    makeTitle("PIZZA PIZZA PIZZA"),
    makeTitle("I am a title"),
    makeTitle("I AM A TITLE"),
    makeTitle("i aM a tITLE"),
    makeTitle("the first letter of every word is capitalized"),
    makeTitle("I Like Pizza"),
    makeTitle("Don't count your ChiCKens BeFore They HatCh"),
    makeTitle("All generalizations are false, including this one"),
    makeTitle("Me and my wife lived happily for twenty years and then we met."),
    makeTitle("There are no stupid questions, just stupid people."),
    makeTitle("1f you c4n r34d 7h15, you r34lly n33d 2 g37 l41d"),
])



const missingNum = (arr) => {
    const n = arr.length + 1;
    const totalSum = (n * (n + 1)) / 2; // Sum of numbers from 1 to n
    const actualSum = arr.reduce((sum, num) => sum + num, 0); // Sum of the given sequence
    return totalSum - actualSum;
}

console.table([
    missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]),
    missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]),
    missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]),
    missingNum([1, 2, 3, 4, 6, 7, 8, 9, 10]),
    missingNum([7, 2, 3, 6, 5, 9, 1, 4, 8]),
    missingNum([7, 2, 3, 9, 4, 5, 6, 8, 10]),
    missingNum([10, 5, 1, 2, 4, 6, 8, 3, 9]),
    missingNum([1, 7, 2, 4, 8, 10, 5, 6, 9]),
])

const removeDups = (arr) => {
    return [...new Set(arr)];
}

console.table([
    removeDups([1, 0, 1, 0]),
    removeDups(["The", "big", "cat"]),
    removeDups(["John", "Taylor", "John"]),
    removeDups(['John', 'Taylor', 'John', 'john']),
    removeDups(['javascript', 'python', 'python', 'ruby', 'javascript', 'c', 'ruby']),
    removeDups([1, 2, 2, 2, 3, 2, 5, 2, 6, 6, 3, 7, 1, 2, 5]),
    removeDups(['#', '#', '%', '&', '#', '$', '&']),
    removeDups([3, 'Apple', 3, 'Orange', 'Apple']),
])

const getBudgets= (Obj) => {
    let totalBudget = 0;
    for(let i = 0; i < Obj.length; i++){
        totalBudget += Obj[i].budget
    }

    return totalBudget
}

console.table([
    getBudgets([{ name: "John", age: 21, budget: 23000 },{ name: "Steve",  age: 32, budget: 40000 },{ name: "Martin",  age: 16, budget: 2700 }]),
    getBudgets([{ name: "John",  age: 21, budget: 29000 },{ name: "Steve",  age: 32, budget: 32000 },{ name: "Martin",  age: 16, budget: 1600 }]),
    getBudgets([{name: "John",  age: 21, budget: 23000}, {name: "Steve",  age: 32, budget: 40000}, {name: "Martin",  age: 16, budget: 2700}]),
    getBudgets([{name: "John",  age: 21, budget: 29000}, {name: "Steve",  age: 32, budget: 32000}, {name: "Martin",  age: 16, budget: 1600}]),
    getBudgets([{name: "John",  age: 21, budget: 19401}, {name: "Steve",  age: 32, budget: 12321}, {name: "Martin",  age: 16, budget: 1204}]),
    getBudgets([{name: "John",  age: 21, budget: 10234}, {name: "Steve",  age: 32, budget: 21754}, {name: "Martin",  age: 16, budget: 4935}]),
])

const isSpecialArray = (arr) => {
    return arr.every((a, b) => a % 2 === b % 2)
}

console.table([
    isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]),
    isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]),
    isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]),
    isSpecialArray([2, 7, 4, 9, 6, 1, 6, 3]),
    isSpecialArray([2, 7, 9, 1, 6, 1, 6, 3]),
    isSpecialArray([2, 7, 8, 8, 6, 1, 6, 3]),
    isSpecialArray([1, 1, 1, 2]),
    isSpecialArray([2, 2, 2, 2]),
    isSpecialArray([2, 1, 2, 1]),
    isSpecialArray([4, 5, 6, 7]),
    isSpecialArray([4, 5, 6, 7, 0, 5])
])

const reverseWords = (arr) => {
    let newArr = arr.split(' ').reverse().join(" ").replace(/\s+/g, ' ').trim()
    return newArr
}

console.table([
    reverseWords(" the sky is blue"),
    reverseWords("hello   world!  "),
    reverseWords("a good example"),
    reverseWords("hello world!"),
    reverseWords("blue is sky the"),
    reverseWords("a good example"),
    reverseWords("fraud! of example another is this"),
    reverseWords("man! the are You"),
])

const inBox = (arr) => {
    for(let i = 0; i < arr.length; i ++){
        if(arr[i][0] === '#' && arr[i][arr[i].length -1] === '#' && arr[i].includes('*')) return true;
    }
    return false;
}

console.table([
    inBox([
        "###",
        "#*#",
        "###"
    ]),

    inBox([
        "####",
        "#* #",
        "#  #",
        "####"
    ]),

    inBox([
        "*####",
        "# #",
        "#  #*",
        "####"
    ]),

    inBox([
        "#####",
        "#   #",
        "#   #",
        "#   #",
        "#####"
    ]),

    inBox([
        "###", 
        "# #", 
        "###"
    ]),

    inBox([
        "####", 
        "#  #", 
        "#  #", 
        "####"
    ]),

    inBox([
        "#####", 
        "#   #", 
        "#   #", 
        "#   #", 
        "#####"
    ]),

    inBox([
        "###", 
        "#*#", 
        "###"
    ]),

    inBox([
        "####", 
        "# *#", 
        "#  #", 
        "####"
    ]),

    inBox([
        "#####", 
        "#  *#", 
        "#   #", 
        "#   #", 
        "#####"
    ]),

    inBox([
        "#####", 
        "#   #", 
        "# * #", 
        "#   #", 
        "#####"
    ]),

    inBox([
        "#####", 
        "#   #", 
        "#   #", 
        "# * #", 
        "#####"
    ]),

    inBox([
        "#####", 
        "#*  #", 
        "#   #", 
        "#   #", 
        "#####"
    ])
])

const awardPrizes = (obj) => {
    let newArr = Object.entries(obj);
    let sortedData = Object.entries(obj).sort((a, b) => b[1] - a[1])

    let fWinner = sortedData[0][0]
    let sWinner = sortedData[1][0]
    let tWinner = sortedData[2][0]

    for(let i = 0; i < newArr.length; i++){
        if(newArr[i][0] === fWinner){
            newArr[i][1] = 'Gold'
        } else if(newArr[i][0] === sWinner){
            newArr[i][1] = 'Silver'
        } else if (newArr[i][0] === tWinner) {
            newArr[i][1] = 'Bronze'
        } else {
            newArr[i][1] = 'Participants'
        }
    }

    return Object.fromEntries(newArr)
}

console.log(awardPrizes({
    "Joshua" : 45,
    "Alex" : 39,
    "Eric" : 43
}))
console.log(awardPrizes({
    "Person A" : 1,
    "Person B" : 2,
    "Person C" : 3,
    "Person D" : 4,
    "Person E" : 102
}))
console.log(awardPrizes({
    "Mario" : 99,
    "Luigi" : 100,
    "Yoshi" : 299,
    "Toad" : 2
}))
console.log(awardPrizes({
    'Joshua' : 45,
    'Alex' : 39,
    'Eric' : 43
}))
console.log(awardPrizes({
    'Person A' : 1,
    'Person B' : 2,
    'Person C' : 3,
    'Person D' : 4,
    'Person E' : 102
}))
console.log(awardPrizes({
    'Mario' : 99,
    'Luigi' : 100,
    'Yoshi' : 299,
    'Toad' : 2
}))


// const number_pairs = (str) => {
//     let jumlahLoop = str.split('').shift()

//     console.log(newArr)
// }

// number_pairs("7 1 2 1 2 1 3 2")