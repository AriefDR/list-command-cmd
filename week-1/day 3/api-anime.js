import fetch from "node-fetch";

const url = 'https://api.jikan.moe/v4/recommendations/anime';
const detail_url = 'https://api.jikan.moe/v4/anime/';


const response = await fetch(url);
const data = await response.json();
const limitedData = data.data.slice(0, 20)
var detailIdAnimeEntries = []

console.log('-----------List All Title -----------------')
limitedData.map((rs) => {
    rs.entry.map((r) => {
        detailIdAnimeEntries.push(r.mal_id) //get detail id
        console.log(r.title)
    })
})


console.log('-----------List Title by Date Release -----------------')
limitedData.sort((a, b) => {
    return new Date(a.date) - new Date(b.date)
}).map((rs) => {
    let day = new Date(rs.date).getDay()
    let m = new Date(rs.date).getMonth()
    let y = new Date(rs.date).getFullYear()
    let hrs = new Date(rs.date).getHours()
    let mnt = new Date(rs.date).getMinutes()
    let sc = new Date(rs.date).getSeconds()
    console.log(`+++++${day}-${m}-${y} ${hrs}:${mnt}:${sc}+++++`)
    rs.entry.map(d => {
        console.log(d.title)
    })
})


let DetailObjectAnime = []
const getPopular = (arr, callback) => {
    arr.map((id, i) => {
        setTimeout(async () => {
            //fetch api
            let detailRes = await fetch(detail_url + id);
            let detailResJson = await detailRes.json();
            // console.log(detailResJson)

            DetailObjectAnime.push(detailResJson)
            if (i == detailIdAnimeEntries.length - 1) {
                callback(DetailObjectAnime)
            }
        }, 5000 * (i + 1))
    })
}

getPopular(detailIdAnimeEntries, async (res) => {
    console.log('---------------Top 5 most popular anime ---------------')
    await res.sort((a, b) => {
        return b.data.popularity - a.data.popularity
    }).slice(0, 5).map(r => {
        console.log(`${r.data.title} - pupularity: ${r.data.popularity}`)
    })

    console.log('----------------- Top 5 High Rank Anime --------------------');
    await res.sort((a, b) => {
        return b.data.rank - a.data.rank
    }).slice(0, 5).map(r => {
        console.log(`${r.data.title} - rank: ${r.data.rank}`)
    })

    console.log('----------------- Most Episodes Anime --------------------');
    await res.sort((a, b) => {
        return b.data.episodes - a.data.episodes
    }).slice(0, 1).map(r => {
        console.log(`${r.data.title} - episodes: ${r.data.episodes}`)
    })
})