let input = 5024

for (let i = 0; i < 10; i++) {  
    setTimeout(() => {
        let hrs = Math.floor((input % (60 * 60 * 24)) / (60 * 60));
        let minutes = Math.floor((input % (60 * 60)) / (60));
        let seconds = Math.floor((input % 60));
    
        hrs = hrs < 10 ? "0" + hrs : hrs;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        console.log(`${hrs} : ${minutes} : ${seconds}`)

        input = input - 1;
    }, 1000 * i)
}