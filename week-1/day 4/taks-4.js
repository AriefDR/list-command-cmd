//Kalkulator
const kalkulator = (a, b, c) => {
    let hasil = 0;
    switch (c.toLowerCase()) {
        case 'add':
            hasil = parseInt(a) + parseInt(b);
            break;
        case 'subtract':
            hasil = a - b;
            break;
        case 'divide':
            hasil = a / b;
            break;
        case 'multiply':
            hasil = a * b;
            break;
        default:
            hasil = undefined
            break;
    }
    return hasil === Infinity ? undefined : hasil
}
console.log("===>Kalkulator<===")
console.table([
    kalkulator("1", "2", "add"),
    kalkulator("4", "5", "subtract"),
    kalkulator("6", "3", "divide"),
    kalkulator("2", "7", "multiply"),
    kalkulator("6", "0", "divide")
])

//cek array yang ada angkanya
const numInStr = (arr) => {
    var result = [];

    for (var i = 0; i < arr.length; i++) {
        var currentStr = arr[i];

        for (var j = 0; j < currentStr.length; j++) {
            if (!isNaN(currentStr[j]) && currentStr[j] !== ' ') {
                result.push(currentStr);
                break;
            }
        }
    }

    return result;
}
console.log("===>Array Number and Char<===")
console.table([numInStr(["1a", "a", "2b", "b"]),
    numInStr(["abc", "abc10"]),
    numInStr(["abc", "ab10c", "a10bc", "bcd"]),
    numInStr(["this is a test", "test1"]),
    numInStr(['abc', 'ab10c', 'a10bc', 'bcd']),
    numInStr(['1', 'a', ' ', 'b']),
    numInStr(['rct', 'ABC', 'Test', 'xYz'])
])

//convert char into number
const alphaVal = (s) => s.toLowerCase().charCodeAt(0) - 97 + 1;
const balanced = (str) => {
    let divideTwo = Math.floor(str.length / 2)
    let strFront = str.slice(0, divideTwo)
    let strEnd = str.length % 2 == 0 ? str.slice(divideTwo) : str.slice((-1) * divideTwo)

    let numberStrFront = 0
    let numberStrEnd = 0

    for (let i = 0; i < divideTwo; i++) {
        numberStrFront += alphaVal(strFront[i])
        numberStrEnd += alphaVal(strEnd[i])
    }

    return numberStrFront === numberStrEnd
}
console.log("=======>Check left and right Score<=======")
console.table([
    balanced("zips"),
    balanced("brake"),
    balanced('at'),
    balanced('forgetful'),
    balanced('vegetation'), balanced('disillusioned'),
    balanced('abstract'), balanced('clever'),
    balanced('conditionalities'), balanced('seasoning'),
    balanced('uptight'),
    balanced('ticket'),
    balanced('calculate'),
    balanced('measure'),
    balanced('join'),
    balanced('anesthesiologies'),
    balanced('command'),
    balanced('graphite'),
    balanced('quadratically'),
    balanced('right'),
    balanced('fossil'),
    balanced('sparkling'),
    balanced('dolphin'),
    balanced('baseball'),
    balanced('immense'),
    balanced('pattern'),
    balanced('hand'),
    balanced('radar'),
    balanced('oven'),
    balanced('immutability'),
    balanced('kayak'),
    balanced('bartender'),
    balanced('weight'),
    balanced('lightbulbs'),
    balanced('tail'),
])

const game_2048 = async (arr) => {
    const sortArr = await arr.sort((a, b) => {
        if (b === 1) return -1;
    });
    await sortArr.forEach((val, i, arr) => {
        if (arr[i] === arr[i + 1]) {
            arr[i] += arr[i + 1];
            arr[i + 1] = 0;
        }
    });

    await sortArr.sort((a, b) => {
        if (b === 0) return -1; // Tuker Posisi
    });
    
    console.log(sortArr)
}

console.log('=======> Game 2408 <=======')
    game_2048([2, 2, 2, 0])
    game_2048([2, 2, 4, 4, 8, 8])
    game_2048([0, 2, 0, 2, 4])
    game_2048([0, 2, 2, 8, 8, 8])
    game_2048([2, 2, 2, 0])
    game_2048([2, 2, 4, 4, 8, 8])
    game_2048([0, 2, 0, 2, 4])
    game_2048([0, 2, 2, 8, 8, 8])
    game_2048([0, 0, 0, 0])
    game_2048([0, 0, 0, 2])
    game_2048([2, 0, 0, 0])
    game_2048([8, 2, 2, 4])
    game_2048([1024, 1024, 1024, 512, 512, 256, 256, 128, 128, 64, 32, 32])