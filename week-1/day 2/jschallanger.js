/*----------------- Javascript Fundamentals ------------------*/
//1

function myFunction(a, b) {
  return a + b;
}

//2

function myFunction(a, b) {
  return a === b;
}

//3

function myFunction(a) {
  return typeof a;
}

//4

function myFunction(a, n) {
  return a.charAt(n - 1);
}

//5

function myFunction(a) {
  return a.slice(3);
}

//6

function myFunction(str) {
  return str.slice(-3);
}

//7

function myFunction(a) {
  return a.slice(0, 3);
}

//8

function myFunction(a) {
  return a.indexOf("is");
}

//9

function myFunction(a) {
  return a.slice(0, a.length / 2);
}

//10

function myFunction(a) {
  return a.slice(0, -3);
}

//11

function myFunction(a, b) {
  return (b / 100) * a;
}

//12

function myFunction(a, b, c, d, e, f) {
  return (((a + b - c) * d) / e) ** f;
}

//13

function myFunction(a, b) {
  return a.includes(b) ? b + a : a + b
}

//14

function myFunction(a) {
  return a % 2 === 0;
}

//15

function myFunction(a, b) {
  return b.split(a).length - 1;
}

//16

function myFunction(a) {
  return parseInt(a) === a
}

//17

function myFunction(a, b) {
  return a < b ? a / b : a * b;
}

//18

function myFunction(a) {
  return Number(a.toFixed(2));
}

//19

function myFunction(a) {
  return a.toString().split('').map(r => parseInt(r))
}


/*----- ----------------- Javascript Array ----------------------*/

//1
function myFunction(a, n) {
  return a[n - 1];
}

//2
function myFunction(a) {
  return a.slice(3);
}

//3
function myFunction(a) {
  return a.slice(-3);
}

//4
function myFunction(a) {
  return a.slice(0, 3);
}

//5
function myFunction(a, n) {
  return a.slice(-n);
}

//6
function myFunction(a, b) {
  return a.filter(del => b !== del)
}

//7
function myFunction(a) {
  return a.length;
}

//8
function myFunction(a) {
  return a.filter((neg) => neg < 0).length;
}

//9
function myFunction(arr) {
  return arr.sort()
}

//10
function myFunction(arr) {
  return arr.sort((a, b) => b - a)
}

//11
function myFunction(a) {
  return a.reduce((acc, cur) => acc + cur, 0);
}

//12
function myFunction(arr) {
  return arr.reduce((acc, cur) => acc + cur, 0) / arr.length
}

//13
function myFunction(arr) {
  return arr.reduce((a, b) => a.length <= b.length ? b : a)
}

//14
function myFunction(arr) {
  return new Set(arr).size === 1
}

//15
function myFunction(...arrays) {
  return arrays.flat()
}

//16
function myFunction(arr) {
  return arr.sort((a, b) => a.b - b.b)
}

//17
function myFunction(a, b) {
  return [...new Set([...a, ...b])].sort((x, y) => x - y);
}

/* --------------------- Javascript Object ------------------*/

//1
function myFunction(obj) {
  return obj.country
}

//2
function myFunction(obj) {
  return obj['prop-2']
}

//3
function myFunction(obj, key) {
  return obj[key]
}

//4
function myFunction(a, b) {
  return b in a;
}

//5
function myFunction(a, b) {
  return Boolean(a[b]);
}

//6
function myFunction(a) {
  return {
    key: a
  };
}

//7
function myFunction(a, b) {
  return {
    [a]: b
  };
}

//8
function myFunction(a, b) {
  let dict = {};
  for (let i = 0; i < a.length; i++) {
    dict[a[i]] = b[i];
  }
  return dict;
}

//9
function myFunction(a) {
  return Object.keys(a);
}

//10
function myFunction(obj) {
  return obj.a && obj.a.b ? obj.a.b : undefined
}

//11
function myFunction(a) {
  return Object.values(a).reduce((acc, cur) => acc + cur);
}

//12
function myFunction(obj) {
  delete obj['b']
  return obj;
}


//13
function myFunction(x, y) {
  let newY = y
  let newX = x

  newY.d = y.b
  delete y.b

  return {
    ...newX,
    newY
  }
}

//14
function myFunction(a, b) {
  let temArr = [];
  Object.values(a).map(l => {
    temArr.push(l * b);
  });
  let dict = {};
  for (let i = 0; i < temArr.length; i++) {
    dict[Object.keys(a)[i]] = temArr[i];
  }
  return dict;
}

/* ------------------------- Javascript Date ---------------------------*/

//1
function myFunction(a, b) {
  return a.getTime() === b.getTime()
}

//2
function myFunction(a, b) {
  const dif = Math.abs(a - b);
  return Math.ceil(dif / (1000 * 3600 * 24))
}

//3
function myFunction(a, b) {
  return a.getTime() - b.getTime() === 0
}

//4
function myFunction(a, b) {
  return Math.abs(a.getTime() - b.getTime()) / (1000*60) <= 60
}

//5
function myFunction(a, b) {
  return a.getTime() - b.getTime() < 0
}

/* -------------------------------Javascript Set -----------------------------------*/

//1
function myFunction(set, val) {
  return set.has(val);
}

//2
function myFunction(set) {
  return [...set];
}

//3
function myFunction(a, b){
  return new Set([...a, ...b])
}

//4
function myFunction(a, b, c) {
  const set = new Set();
  set.add(a);
  set.add(b);
  set.add(c);
  return set;
}

//5
function myFunction(set, val) {
  set.delete(val);
  return set;
}

//6