const heroObject = require('./hero')

/* 1 display list hero from hero.js */
heroObject.hero.map((hero) => {
    tempName.push(hero.hero_name)
    console.log(hero.hero_name)
})

/* 2 display list hero name from hero.json then sort ascending */
let tempName = []
let sortHeroName = tempName.sort()
for(let i = 0; i < sortHeroName.length; i ++){
    console.log(sortHeroName[i])
}

/* 3 display all hero with role tank */
const heroRoleTank = heroObject.hero.filter(hero => hero.hero_role.includes('Tank'))
console.log(heroRoleTank)

/* 4 display all hero with specially crowd control */
const heroCrowdControl = heroObject.hero.filter(hero => hero.hero_specially.includes('Crowd Control'))
console.log(heroCrowdControl)