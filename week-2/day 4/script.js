let output = '';
const display = document.getElementById('hasil');
const buttons = document.querySelectorAll(".calculator-keys button");
const specialChars = ["*", "/", "-", "+", "="];
let toggleSwitch = document.getElementsByClassName('redButton')[0]
//Define function to calculate based on button clicked.
const calculate = (btnValue) => {
    try {
        display.focus();
        if (btnValue === "=" && output !== "") {
            //If output has '%', replace with '/100' before evaluating.
            output = eval(output.replace("%", "/100"));
        } else if (btnValue === "RESET") {
            output = "";
        } else if (btnValue === "DEL") {
            //If DEL button is clicked, remove the last character from the output.
            output = output.toString().slice(0, -1);
        } else {
            //If output is empty and button is specialChars then return
            if (output === "" && specialChars.includes(btnValue)) return;
            output += btnValue;
        }
        // display.value = output;

        display.innerHTML = output
    } catch (error) {
        display.innerHTML = 'ERROR'
    }
};
//Add event listener to buttons, call calculate() on click.
buttons.forEach((button) => {
    //Button click listener calls calculate() with dataset value as argument.
    button.addEventListener("click", (e) => calculate(e.target.value));
});

function thema(str) {
    switch (str) {
        case '1':
            toggleSwitch.classList.add('horizTranslate1');
            toggleSwitch.classList.remove('horizTranslate3');
            toggleSwitch.classList.remove('horizTranslate2');
            
            document.getElementById("theme-btn").style.backgroundColor = "#DB3D2B"
            document.getElementById("bg-theme").style.backgroundColor = "#4A5B7E"
            document.getElementById("theme-delete").style.backgroundColor = "#4A5B7E"
            document.getElementById("theme-reset").style.backgroundColor = "#4A5B7E"
            document.getElementById("equal-sign").style.backgroundColor = "#DB3D2B"
            break;
        case '2':
            toggleSwitch.classList.add('horizTranslate2');
            toggleSwitch.classList.remove('horizTranslate3');
            toggleSwitch.classList.remove('horizTranslate1');

            document.getElementById("theme-btn").style.backgroundColor = "#eab676"
            document.getElementById("bg-theme").style.backgroundColor = "#76b5c5"
            document.getElementById("theme-delete").style.backgroundColor = "#76b5c5"
            document.getElementById("theme-reset").style.backgroundColor = "#76b5c5"
            document.getElementById("equal-sign").style.backgroundColor = "#eab676"
            break;
        case '3':
            toggleSwitch.classList.add('horizTranslate3');
            toggleSwitch.classList.remove('horizTranslate1');
            toggleSwitch.classList.remove('horizTranslate2');

            document.getElementById("theme-btn").style.backgroundColor = "#8AE633"
            document.getElementById("bg-theme").style.backgroundColor = "#E65233"
            document.getElementById("theme-delete").style.backgroundColor = "#E65233"
            document.getElementById("theme-reset").style.backgroundColor = "#E65233"
            document.getElementById("equal-sign").style.backgroundColor = "#8AE633"
            break;
        default:
            break;
    }
}